![RotAgent](http://i.imgur.com/2OLlDl7.png)
### **About**
Rotation Agent is a rotation profile for **Probably Engine**. Currently supports the following classes:
* Hunter
  * Beast Mastery (Raid Ready)
  * Marksmanship (Raid Ready)
  * ~~Survival~~

### **Requirements**
* *ProbablyEngine* [(latest)](https://gitlab.com/probablyengine/probably/repository/archive.zip)
* *RotAgent* [(latest)](https://gitlab.com/StinkyTwitch/Probably_RotAgent/repository/archive.zip)

### **Installation**
1. Download Probably from the link above.
2. Unzip the file.
3. Rename the "probably.git" folder to "Probably".
4. Move the "Probably" folder to "Interface\AddOns" folder in your WoW directory.
5. Download Probably_RotAgent from the link above.
6. Rename the "Probably_RotAgent.git" folder to "Probably_RotAgent".
7. Move the "Probably_RotAgent" folder to "Interface\AddOns" folder in your WoW directory.

### **Usage**
* Spell Queue
  * For any spell you'd like to make available through the spell queue make a macro in the following fashion:
    * #showtooltip
    * /ra cast Spell Name
  * Spelling matters as well as case sensitivity
* Info Windows
  * /ra info
* More coming soon(tm)