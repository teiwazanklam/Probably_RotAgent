-- LubDraw by docbrown on fh-wow.com
_G["RotDraw"] = _G["RotDraw"] or { }

RotDraw = LibStub:NewLibrary("RotDraw-1.0", 1)
RotDraw.version = 1.1

local WorldToScreen_Original = WorldToScreen;
local function WorldToScreen (wX, wY, wZ)
        local sX, sY = WorldToScreen_Original(wX, wY, wZ);
        if sX and sY then
                return sX, -(WorldFrame:GetTop() - sY);
        else
                return sX, sY;
        end
end

local sin, cos, atan, atan2, sqrt, rad = math.sin, math.cos, math.atan, math.atan2, math.sqrt, math.rad
local tinsert, tremove = tinsert, tremove

RotDraw.line = RotDraw.line or { r = 0, g = 1, b = 0, a = 1, w = 1 }
RotDraw.texture = "Interface\\AddOns\\Probably_RotAgent\\Externals\\RotDraw\\LineTemplate"
RotDraw.level = "BACKGROUND"
RotDraw.callbacks = { }

if not RotDraw.canvas then
	RotDraw.canvas = CreateFrame("Frame", WorldFrame)
	RotDraw.canvas:SetAllPoints(WorldFrame)
	RotDraw.textures = { }
	RotDraw.textures_used = { }
	RotDraw.fontstrings = { }
	RotDraw.fontstrings_used = { }
end

function RotDraw.SetColor(r, g, b, a)
	RotDraw.line.r = r * 0.00390625
	RotDraw.line.g = g * 0.00390625
	RotDraw.line.b = b * 0.00390625
	if a then
		RotDraw.line.a = a * 0.01
	else
		RotDraw.line.a = 1
	end
end

function RotDraw.SetColorRaw(r, g, b, a)
	RotDraw.line.r = r
	RotDraw.line.g = g
	RotDraw.line.b = b
	RotDraw.line.a = a
end

function RotDraw.SetWidth(w)
	RotDraw.line.w = w
end

function RotDraw.Line(sx, sy, sz, ex, ey, ez)
	if not FireHack then return end

	local sx, sy = WorldToScreen(sx, sy, sz)
	local ex, ey = WorldToScreen(ex, ey, ez)

	RotDraw.Draw2DLine(sx, sy, ex, ey)
end

function RotDraw.rotateX(cx, cy, cz, px, py, pz, r)
	if r == nil then return px, py, pz end
	local s = sin(r)
	local c = cos(r)
	-- center of rotation
	px, py, pz = px - cx,  py - cy, pz - cz
	local x = px + cx
	local y = ((py * c - pz * s) + cy)
	local z = ((py * s + pz * c) + cz)
	return x, y, z
end

function RotDraw.rotateY(cx, cy, cz, px, py, pz, r)
	if r == nil then return px, py, pz end
	local s = sin(r)
	local c = cos(r)
	-- center of rotation
	px, py, pz = px - cx,  py - cy, pz - cz
	local x = ((pz * s + px * c) + cx)
	local y = py + cy
	local z = ((pz * c - px * s) + cz)
	return x, y, z
end

function RotDraw.rotateZ(cx, cy, cz, px, py, pz, r)
	if r == nil then return px, py, pz end
	local s = sin(r)
	local c = cos(r)
	-- center of rotation
	px, py, pz = px - cx,  py - cy, pz - cz
	local x = ((px * c - py * s) + cx)
	local y = ((px * s + py * c) + cy)
	local z = pz + cz
	return x, y, z
end

function RotDraw.Array(vectors, x, y, z, rotationX, rotationY, rotationZ)
	for _, vector in ipairs(vectors) do
		local sx, sy, sz = x+vector[1], y+vector[2], z+vector[3]
		local ex, ey, ez = x+vector[4], y+vector[5], z+vector[6]

		if rotationX then
			sx, sy, sz = RotDraw.rotateX(x, y, z, sx, sy, sz, rotationX)
			ex, ey, ez = RotDraw.rotateX(x, y, z, ex, ey, ez, rotationX)
		end
		if rotationY then
			sx, sy, sz = RotDraw.rotateY(x, y, z, sx, sy, sz, rotationY)
			ex, ey, ez = RotDraw.rotateY(x, y, z, ex, ey, ez, rotationY)
		end
		if rotationZ then
			sx, sy, sz = RotDraw.rotateZ(x, y, z, sx, sy, sz, rotationZ)
			ex, ey, ez = RotDraw.rotateZ(x, y, z, ex, ey, ez, rotationZ)
		end

		local sx, sy = WorldToScreen(sx, sy, sz)
		local ex, ey = WorldToScreen(ex, ey, ez)
		RotDraw.Draw2DLine(sx, sy, ex, ey)
	end
end

local LINEFACTOR = 256/254;
local LINEFACTOR_2 = LINEFACTOR / 2;

function RotDraw.Draw2DLine(sx, sy, ex, ey)

	if not FireHack or not sx or not sy or not ex or not ey then return end
	local T = tremove(RotDraw.textures) or false
	if T == false then
		T = RotDraw.canvas:CreateTexture(nil, "BACKGROUND")
		T:SetDrawLayer(RotDraw.level)
	end
	T:SetTexture(RotDraw.texture)
	tinsert(RotDraw.textures_used, T)

	-- Determine dimensions and center point of line
	local dx, dy = ex - sx, ey - sy
	local cx, cy = (sx + ex) * 0.5, (sy + ey) * 0.5

	-- Normalize direction if necessary
	if (dx < 0) then
	  dx,dy = -dx, -dy
	end

	-- Calculate actual length of line
	local l = sqrt((dx * dx) + (dy * dy))

	-- Quick escape if it's zero length
	if (l == 0) then
		T:SetTexCoord(0,0,0,0,0,0,0,0)
		T:SetPoint("BOTTOMLEFT", C, "TOPLEFT", cx,cy)
		T:SetPoint("TOPRIGHT",   C, "TOPLEFT", cx,cy)
		return
	end

	-- Sin and Cosine of rotation, and combination (for later)
	local s,c = -dy / l, dx / l
	local sc = s * c

	-- Calculate bounding box size and texture coordinates
	local Bwid, Bhgt, BLx, BLy, TLx, TLy, TRx, TRy, BRx, BRy
	if (dy >= 0) then
		Bwid = ((l * c) - (RotDraw.line.w * s)) * LINEFACTOR_2
		Bhgt = ((RotDraw.line.w * c) - (l * s)) * LINEFACTOR_2
		BLx, BLy, BRy = (RotDraw.line.w / l) * sc, s * s, (l / RotDraw.line.w) * sc
		BRx, TLx, TLy, TRx = 1 - BLy, BLy, 1 - BRy, 1 - BLx
		TRy = BRx;
	else
		Bwid = ((l * c) + (RotDraw.line.w * s)) * LINEFACTOR_2
		Bhgt = ((RotDraw.line.w * c) + (l * s)) * LINEFACTOR_2
		BLx, BLy, BRx = s * s, -(l / RotDraw.line.w) * sc, 1 + (RotDraw.line.w / l) * sc
		BRy, TLx, TLy, TRy = BLx, 1 - BRx, 1 - BLx, 1 - BLy
		TRx = TLy
	end

	if TLx > 10000 then TLx = 10000 elseif TLx < -10000 then TLx = -10000 end
	if TLy > 10000 then TLy = 10000 elseif TLy < -10000 then TLy = -10000 end
	if BLx > 10000 then BLx = 10000 elseif BLx < -10000 then BLx = -10000 end
	if BLy > 10000 then BLy = 10000 elseif BLy < -10000 then BLy = -10000 end
	if TRx > 10000 then TRx = 10000 elseif TRx < -10000 then TRx = -10000 end
	if TRy > 10000 then TRy = 10000 elseif TRy < -10000 then TRy = -10000 end
	if BRx > 10000 then BRx = 10000 elseif BRx < -10000 then BRx = -10000 end
	if BRy > 10000 then BRy = 10000 elseif BRy < -10000 then BRy = -10000 end

	T:ClearAllPoints()

	-- Set texture coordinates and anchors
	T:SetTexCoord(TLx, TLy, BLx, BLy, TRx, TRy, BRx, BRy)
	T:SetPoint("BOTTOMLEFT", RotDraw.canvas, "TOPLEFT", cx - Bwid, cy - Bhgt)
	T:SetPoint("TOPRIGHT",   RotDraw.canvas, "TOPLEFT", cx + Bwid, cy + Bhgt)
	T:SetVertexColor(RotDraw.line.r, RotDraw.line.g, RotDraw.line.b, RotDraw.line.a)
	T:Show()

end

local full_circle = rad(365)
local small_circle_step = rad(12)

function RotDraw.Circle(x, y, z, size)
	local lx, ly, nx, ny, fx, fy = false, false, false, false, false, false
	for v=0, full_circle, small_circle_step do
		nx, ny = WorldToScreen( (x+cos(v)*size), (y+sin(v)*size), z )
		if lx and ly then
			RotDraw.Draw2DLine(lx, ly, nx, ny)
		else
			fx, fy = nx, ny
		end
		lx, ly = nx, ny
	end
end

function RotDraw.Arc(x, y, z, size, arc, rotation)
	local lx, ly, nx, ny, fx, fy = false, false, false, false, false, false
	local half_arc = arc * 0.5
	local ss = (arc/half_arc)
	local as, ae = -half_arc, half_arc
	for v = as, ae, ss do
		nx, ny = WorldToScreen( (x+cos(rotation+rad(v))*size), (y+sin(rotation+rad(v))*size), z )
		if lx and ly then
			RotDraw.Draw2DLine(lx, ly, nx, ny)
		else
			fx, fy = nx, ny
		end
		lx, ly = nx, ny
	end
	local px, py = WorldToScreen(x, y, z)
	RotDraw.Draw2DLine(px, py, lx, ly)
	RotDraw.Draw2DLine(px, py, fx, fy)
end

function RotDraw.Texture(config, x, y, z, alphaA)

	local texture, width, height = config.texture, config.width, config.height
	local left, right, top, bottom, scale =  config.left, config.right, config.top, config.bottom, config.scale
	local alpha = config.alpha or alphaA

	if not FireHack or not texture or not width or not height or not x or not y or not z then return end
	if not left or not right or not top or not bottom then
		left = 0
		right = 1
		top = 0
		bottom = 1
	end
	if not scale then
		local cx, cy, cz = GetCameraPosition()
		scale = width / RotDraw.Distance(x, y, z, cx, cy, cz)
	end

	local sx, sy = WorldToScreen(x, y, z)
	if not sx or not sy then return end
	local w = width * scale
	local h = height * scale
	sx = sx - w*0.5
	sy = sy + h*0.5
	local ex, ey = sx + w, sy - h

	local T = tremove(RotDraw.textures) or false
	if T == false then
		T = RotDraw.canvas:CreateTexture(nil, "BACKGROUND")
		T:SetDrawLayer(RotDraw.level)
		T:SetTexture(RotDraw.texture)
	end
	tinsert(RotDraw.textures_used, T)
	T:ClearAllPoints()
	T:SetTexCoord(left, right, top, bottom)
	T:SetTexture(texture)
	T:SetWidth(width)
	T:SetHeight(height)
	T:SetPoint("TOPLEFT", RotDraw.canvas, "TOPLEFT", sx, sy)
	T:SetPoint("BOTTOMRIGHT", RotDraw.canvas, "TOPLEFT", ex, ey)
	T:SetVertexColor(1, 1, 1, 1)
	if alpha then T:SetAlpha(alpha) else T:SetAlpha(1) end
	T:Show()

end

function RotDraw.Text(text, font, x, y, z)

	local sx, sy = WorldToScreen(x, y, z)

	if sx and sy then

		local F = tremove(RotDraw.fontstrings) or RotDraw.canvas:CreateFontString(nil, "BACKGROUND")

		F:SetFontObject(font)
		F:SetText(text)
		F:SetTextColor(RotDraw.line.r, RotDraw.line.g, RotDraw.line.b, RotDraw.line.a)

		if p then
			local width = F:GetStringWidth() - 4
			local offsetX = width*0.5
			local offsetY = F:GetStringHeight() + 3.5
			local pwidth = width*p*0.01
			FHAugment.drawLine(sx-offsetX, sy-offsetY, (sx+offsetX), sy-offsetY, 4, r, g, b, 0.25)
			FHAugment.drawLine(sx-offsetX, sy-offsetY, (sx+offsetX)-(width-pwidth), sy-offsetY, 4, r, g, b, 1)
		end

		F:SetPoint("TOPLEFT", UIParent, "TOPLEFT", sx-(F:GetStringWidth()*0.5), sy)
		F:Show()

		tinsert(RotDraw.fontstrings_used, F)

	end

end

local rad90 = math.rad(-90)

function RotDraw.Box(x, y, z, width, height, rotation, offset_x, offset_y)

	if not offset_x then offset_x = 0 end
	if not offset_y then offset_y = 0 end

	if rotation then rotation = rotation + rad90 end

	local half_width = width * 0.5
	local half_height = height * 0.5

	local p1x, p1y = RotDraw.rotateZ(x, y, z, x - half_width + offset_x, y - half_width + offset_y, z, rotation)
	local p2x, p2y = RotDraw.rotateZ(x, y, z, x + half_width + offset_x, y - half_width + offset_y, z, rotation)
	local p3x, p3y = RotDraw.rotateZ(x, y, z, x - half_width + offset_x, y + half_width + offset_y, z, rotation)
	local p4x, p4y = RotDraw.rotateZ(x, y, z, x - half_width + offset_x, y - half_width + offset_y, z, rotation)
	local p5x, p5y = RotDraw.rotateZ(x, y, z, x + half_width + offset_x, y + half_width + offset_y, z, rotation)
	local p6x, p6y = RotDraw.rotateZ(x, y, z, x + half_width + offset_x, y - half_width + offset_y, z, rotation)
	local p7x, p7y = RotDraw.rotateZ(x, y, z, x - half_width + offset_x, y + half_width + offset_y, z, rotation)
	local p8x, p8y = RotDraw.rotateZ(x, y, z, x + half_width + offset_x, y + half_width + offset_y, z, rotation)

	RotDraw.Line(p1x, p1y, z, p2x, p2y, z)
	RotDraw.Line(p3x, p3y, z, p4x, p4y, z)
	RotDraw.Line(p5x, p5y, z, p6x, p6y, z)
	RotDraw.Line(p7x, p7y, z, p8x, p8y, z)

end

local deg45 = math.rad(45)
local arrowX = {
	{ 0  , 0, 0, 1.5,  0,    0   },
	{ 1.5, 0, 0, 1.2,  0.2, -0.2 },
	{ 1.5, 0, 0, 1.2, -0.2,  0.2 }
}
local arrowY = {
	{ 0, 0  , 0,  0  , 1.5,  0   },
	{ 0, 1.5, 0,  0.2, 1.2, -0.2 },
	{ 0, 1.5, 0, -0.2, 1.2,  0.2 }
}
local arrowZ = {
	{ 0, 0, 0  ,  0,    0,   1.5 },
	{ 0, 0, 1.5,  0.2, -0.2, 1.2 },
	{ 0, 0, 1.5, -0.2,  0.2, 1.2 }
}

function RotDraw.DrawHelper()
	local playerX, playerY, playerZ = ObjectPosition("player")
	local old_red, old_green, old_blue, old_alpha, old_width = RotDraw.line.r, RotDraw.line.g, RotDraw.line.b, RotDraw.line.a, RotDraw.line.w

	-- X
	RotDraw.SetColor(255, 0, 0, 100)
	RotDraw.SetWidth(1)
	RotDraw.Array(arrowX, playerX, playerY, playerZ, deg45, false, false)
	RotDraw.Text('X', "GameFontNormal", playerX + 1.75, playerY, playerZ)
	-- Y
	RotDraw.SetColor(0, 255, 0, 100)
	RotDraw.SetWidth(1)
	RotDraw.Array(arrowY, playerX, playerY, playerZ, false, -deg45, false)
	RotDraw.Text('Y', "GameFontNormal", playerX, playerY + 1.75, playerZ)
	-- Z
	RotDraw.SetColor(0, 0, 255, 100)
	RotDraw.SetWidth(1)
	RotDraw.Array(arrowZ, playerX, playerY, playerZ, false, false, false)
	RotDraw.Text('Z', "GameFontNormal", playerX, playerY, playerZ + 1.75)

	RotDraw.line.r, RotDraw.line.g, RotDraw.line.b, RotDraw.line.a, RotDraw.line.w = old_red, old_green, old_blue, old_alpha, old_width
end

function RotDraw.Distance(ax, ay, az, bx, by, bz)
	return math.sqrt(((bx-ax)*(bx-ax)) + ((by-ay)*(by-ay)) + ((bz-az)*(bz-az)))
end

function RotDraw.Camera()
	local fX, fY, fZ = ObjectPosition("player")
	local sX, sY, sZ = GetCameraPosition()
	return sX, sY, sZ, atan2(sY - fY, sX - fX), atan((sZ - fZ) / sqrt(((fX - sX) ^ 2) + ((fY - sY) ^ 2)))
end

function RotDraw.Sync(callback)
	tinsert(RotDraw.callbacks, callback)
end

function RotDraw.clearCanvas()
	-- RotDraw.stats = #RotDraw.textures_used
	for i = #RotDraw.textures_used, 1, -1 do
		RotDraw.textures_used[i]:Hide()
		tinsert(RotDraw.textures, tremove(RotDraw.textures_used))
	end
	for i = #RotDraw.fontstrings_used, 1, -1 do
		RotDraw.fontstrings_used[i]:Hide()
		tinsert(RotDraw.fontstrings, tremove(RotDraw.fontstrings_used))
	end
end

local function OnUpdate()
	RotDraw.clearCanvas()
	for _, callback in ipairs(RotDraw.callbacks) do
		callback()
		if RotDraw.helper then
			RotDraw.DrawHelper()
		end
		RotDraw.helper = false
	end
end

C_Timer.NewTicker(0.01, OnUpdate)

--RotDraw.canvas:SetScript("OnUpdate", OnUpdate)