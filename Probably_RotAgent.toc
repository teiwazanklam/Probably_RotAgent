## Interface: 60200
## Title: Probably_RotAgent
## Notes: RotAgent Rotation Helper for ProbablyEngine
## Author: StinkyTwitch
## Version: 0.3.5
## Dependencies: Probably

Externals\RotDraw\LibStub.lua
Externals\RotDraw\RotDraw.lua
Externals\Soapbox\Soapbox.lua

Libs\Globals.lua
Libs\LibCommands.lua
Libs\LibDK.lua
Libs\LibHunter.lua
Libs\LibPE.lua
Libs\LibUnitCacheManager.lua

Libs\HelperSymbols.lua
Libs\HelperWindowTarget.lua
Libs\HelperWindowVariables.lua
Libs\HelperWindowStatus.lua
Libs\HelperWindowCache.lua

Rotations\DeathKnightFrostDW.lua
Rotations\HunterBeastMastery.lua
Rotations\HunterMarksmanship.lua

Plugins\RotFishing.lua

Libs\Timers.lua