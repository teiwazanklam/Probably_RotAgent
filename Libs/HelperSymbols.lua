--[[------------------------------------------------------------------------------------------------

HelperSymbols.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]
--local LibDraw = LibStub("LibDraw-1.0")

RA.helperSymbols = false

function RA.HelperSymbolsToggle()
	RA.helperSymbols = not RA.helperSymbols

	if RA.helperSymbols then
		print("Helper Symbols ON!")
	else
		print("Helper Symbols OFF!")
	end
end


RotDraw.Sync(function()

	if FireHack and RA.helperSymbols then


		-- Pet Target Identifier -------------------------------------------------------------------
		if UnitExists("pettarget") then
			local targetX, targetY, targetZ = ObjectPosition("pettarget")

			RotDraw.SetColor(252, 255, 150)
			RotDraw.Circle(targetX, targetY, targetZ, 0.9)
		end


		-- Target Identifier -----------------------------------------------------------------------
		if UnitExists("target") then
			local targetX, targetY, targetZ = ObjectPosition("target")

			RotDraw.SetColor(100, 246, 255)
			RotDraw.Circle(targetX, targetY, targetZ, 1.0)
		end


	end
end)