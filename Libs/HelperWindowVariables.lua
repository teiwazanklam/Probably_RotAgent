--[[------------------------------------------------------------------------------------------------

HelperWindowVariables.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]


--[[------------------------------------------------------------------------------------------------
    Name: helperWindowVariablesGUI
    Type: Diesal GUI config table
    Arguments:  None
    Return: None
    Description:
--]]
RA.helperWindowVariablesGUI = ProbablyEngine.interface.buildGUI({
    key = "helperWindowCacheGUI_config",
    title = 'RotAgent',
    subtitle = 'Important Variables',
    width = 227,
    height = 285,
    resize = true,
    color = "4e7300",
    config = {
        { key = 'unitcachecount', type = "text", text = "random", size = 12, align = "left", offset = 0 },
        { key = 'unitcacheallcount', type = "text", text = "random", size = 12, align = "left", offset = 0 },
        { key = 'unitsaroundunitcount', type = "text", text = "random", size = 12, align = "left", offset = 0 },
        { type = 'rule', },
        { key = 'executeunit', type = "text", text = "random", size = 12, align = "left", offset = 0 },
        { key = 'highestunit', type = "text", text = "random", size = 12, align = "left", offset = 0 },
        { key = 'lowestunit', type = "text", text = "random", size = 12, align = "left", offset = 0 },
        { key = 'nearestunit', type = "text", text = "random", size = 12, align = "left", offset = 0 },
        { type = 'rule', },
        { key = 'ttdtrackersize', type = "text", text = "random", size = 12, align = "left", offset = 0 },
    }
})
RA.helperWindowVariablesGUI.parent:Hide()

RA.helperWindowVariablesGUI.parent.frame:SetPoint("TOPLEFT", RA.helperWindowTargetGUI.parent.frame, "TOPRIGHT", 3, 0)
RA.helperWindowVariablesGUI.parent.frame:SetPoint("BOTTOMLEFT", RA.helperWindowTargetGUI.parent.frame, "BOTTOMRIGHT", 3, 0)

--[[------------------------------------------------------------------------------------------------
    Name: RA.helperWindowVariablesShow
    Type: Function
    Arguments: None
    Return: None
    Description:
--]]
RA.helperWindowVariablesShow = nil
function RA.HelperWindowVariablesShow()
    RA.helperWindowVariablesShow = not RA.helperWindowVariablesShow

    if RA.helperWindowVariablesShow then
        RA.helperWindowVariablesGUI.parent:Show()
    else
        RA.helperWindowVariablesGUI.parent:Hide()
    end
end


--[[------------------------------------------------------------------------------------------------
    Name: RA.HelperWindowObjectCacheUpdate
    Type: Function
    Arguments:  None
    Return: None
    Description:
--]]
function RA.HelperWindowVariablesUpdate()
    if RA.helperWindowVariablesShow then
        RA.helperWindowVariablesGUI.elements.unitcachecount:SetText("\124cffFFFFFFCache Count \124cff666666= \124cff4e7300" .. tostring(#RA.unitCache))
        RA.helperWindowVariablesGUI.elements.unitcacheallcount:SetText("\124cffFFFFFFCacheAll Count \124cff666666= \124cff4e7300" .. tostring(#RA.unitCacheAll))
        RA.helperWindowVariablesGUI.elements.unitsaroundunitcount:SetText("\124cffFFFFFFUnitsAroundUnit Count \124cff666666= \124cff4e7300" .. tostring(RA.uauCacheSize))
        RA.helperWindowVariablesGUI.elements.executeunit:SetText("\124cffFFFFFFExecute Unit \124cff666666= \124cff4e7300" .. tostring(RA.executeUnitPercentage) .." \124cff666666 %")
        RA.helperWindowVariablesGUI.elements.highestunit:SetText("\124cffFFFFFFHighest Unit \124cff666666= \124cff4e7300" .. tostring(RA.highestUnitHealth) .. " \124cff666666 hp")
        RA.helperWindowVariablesGUI.elements.lowestunit:SetText("\124cffFFFFFFLowest Unit \124cff666666= \124cff4e7300" .. tostring(RA.lowestUnitHealth).. " \124cff666666 hp")
        RA.helperWindowVariablesGUI.elements.nearestunit:SetText("\124cffFFFFFFNearest Unit \124cff666666= \124cff4e7300" .. tostring(RA.nearestUnitDistance) .. " \124cff666666 yds")
        RA.helperWindowVariablesGUI.elements.ttdtrackersize:SetText("\124cffFFFFFFTimeToDieTrack Count \124cff666666= \124cff4e7300" .. tostring(#RA.timeToDeathTrack))
    end
end