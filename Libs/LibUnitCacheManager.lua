--[[------------------------------------------------------------------------------------------------

LibUnitCacheManager.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]
--local RAName, RA = ...

RA.unitCacheAll = { }
RA.unitCache = { }


local tableStorageCacheAll = { }  -- Create a table that will store unused table memory addresses
local tableStorageCache = { }  -- Create a table that will store unused table memory addresses


local function ClearUnitCacheAll()
	for i = #RA.unitCacheAll, 1, -1 do
		tinsert(tableStorageCacheAll, tremove(RA.unitCacheAll))
	end
end
local function ClearUnitCache()
	for i = #RA.unitCache, 1, -1 do
		tinsert(tableStorageCache, tremove(RA.unitCache))
	end
end


local function GetTableCacheAll()
	local t = tremove(tableStorageCacheAll)
	if t ~= nil then
		tinsert(RA.unitCacheAll, t)
		return t
	else
		t = { }
		tinsert(RA.unitCacheAll, t)
		return t
	end
end
local function GetTableCache()
	local t = tremove(tableStorageCache)
	if t ~= nil then
		tinsert(RA.unitCache, t)
		return t
	else
		t = { }
		tinsert(RA.unitCache, t)
		return t
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: UnitCacheManager
	Type: Function
	Arguments: range - int variable representing radius in yards to search from 'player'
	Return: None
	Description:
--]]
function RA.UnitCacheManager(range)
	ClearUnitCacheAll()
	ClearUnitCache()

	RA.executeUnit = nil
    RA.executeUnitPercentage = nil
    RA.highestUnit = nil
    RA.highestUnitHealth = nil
    RA.lowestUnit = nil
    RA.lowestUnitHealth = nil
    RA.nearestUnit = nil
    RA.nearestUnitDistance = nil

	if range == nil then
		range = 40
	end

	if FireHack and ProbablyEngine.module.player.combat then
		local objectCount = GetObjectCount()

		for i=1, objectCount do
			local object = GetObjectWithIndex(i)
			local objectExists = ObjectExists(object)

			if objectExists then
				local objectType = ObjectIsType(object, ObjectTypes.Unit)

				if objectType then
					local objectDistance = RA.Distance("player", object, 2, "reach")

					if objectDistance <= range then
						local blackListed = RA.SpecialBlackListTarget(object)
						local objectHealth = UnitHealth(object)
						local objectIsDead = UnitIsDead(object)
						local objectAttackable = UnitCanAttack("player", object)

						if objectHealth > 0 and objectAttackable and not objectIsDead and not blackListed then
							local objectHealthMax = UnitHealthMax(object)
							local objectHealthPercentage = math.floor((objectHealth / objectHealthMax) * 100)
							local objectName = UnitName(object)
							local objectReaction = UnitReaction("player", object)

							if objectReaction and objectReaction <= 4 then
								local specialEnemy = RA.SpecialEnemyTarget(object)
								local ccDebuff = RA.SpecialCCDebuff(object)
								local immuneBuff = RA.SpecialImmuneBuff(object)
								local tappedByPlayer = UnitIsTappedByPlayer(object)
								local tappedByAll = UnitIsTappedByAllThreatList(object)

								local tableCacheAll = GetTableCacheAll()
								tableCacheAll.object = object

								if not ccDebuff and not immuneBuff and (tappedByPlayer or tappedByAll or specialEnemy) then

									local tableCache = GetTableCache()
									tableCache.object = object
									tableCache.name = objectName
									tableCache.health = objectHealthPercentage
									tableCache.distance = objectDistance

									local los = LineOfSight("player", object)
									if los then
										local executeRange = RA.Fetch("executevalue", "20")
                                        if RA.executeUnitPercentage == nil
                                            and objectHealthPercentage <= executeRange
                                        then
                                            RA.executeUnit = object
                                            RA.executeUnitPercentage = objectHealthPercentage
                                        elseif RA.executeUnitPercentage ~= nil
                                            and objectHealthPercentage >= RA.executeUnitPercentage
                                            and objectHealthPercentage <= executeRange
                                        then
                                            RA.executeUnit = object
                                            RA.executeUnitPercentage = objectHealthPercentage
                                        end

										if RA.lowestUnitHealth == nil then
											RA.lowestUnitHealth = objectHealth
											RA.lowestUnit = object
										elseif objectHealth < RA.lowestUnitHealth then
											RA.lowestUnitHealth = objectHealth
											RA.lowestUnit = object
										end

										if RA.highestUnitHealth == nil then
											RA.highestUnitHealth = objectHealth
											RA.highestUnit = object
										elseif objectHealth > RA.highestUnitHealth then
											RA.highestUnitHealth = objectHealth
											RA.highestUnit = object
										end

										if RA.nearestUnitDistance == nil then
											RA.nearestUnitDistance = objectDistance
											RA.nearestUnit = object
										elseif objectDistance < RA.nearestUnitDistance then
											RA.nearestUnitDistance = objectDistance
											RA.nearestUnit = object
										end
									end
								end
							end
						end
					end
				end
			end
		end
	else
		if FireHack and not ProbablyEngine.module.player.combat then
			return

		-- Generic Unlocker
		else
			if ProbablyEngine.module.player.combat then
				if UnitExists("target") then
					local object = UnitGUID("target")
					local objectHealth = UnitHealth("target")
					local objectHealthMax = UnitHealthMax("target")
					local objectHealthPercentage = math.floor((objectHealth / objectHealthMax) * 100)
					local objectName = UnitName("target")
					local objectDistance = 0

					RA.unitCacheAll[#RA.unitCacheAll+1] = { object = object }
					RA.unitCache[#RA.unitCache+1] = {
						object = object,
						name = objectName,
						health = objectHealthPercentage,
						distance = objectDistance
					}
				end
				return
			else
				return
			end
		end
	end
end