--[[------------------------------------------------------------------------------------------------

HunterBeastMastery.lua

RotAgent (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

------------------------------------------------------------------------------------------------]]--


----------------------------------------------------------------------------------------------------
--  LOCAL VARIABLS/TABLES                                                                         --
----------------------------------------------------------------------------------------------------
local defensives = RA.hunterDefensives
local miscellaneousCombat = RA.hunterMiscellaneousCombat
local miscellaneousOOC = RA.hunterMiscellaneousOOC
local misdirection = RA.hunterMisdirection
local petAttack = RA.hunterPetAttack
local petManagementCombat = RA.hunterPetManagementCombat
local petManagementOOC = RA.hunterPetManagementOOC
local petSummon = RA.hunterPetSummon
local poolFocusFocusingShot = RA.hunterPoolFocusFocusingShot
local poolFocusSteadyShot = RA.hunterPoolFocusSteadyShot
local queueSpells = RA.hunterQueueSpellsMM
--local rotOverloads = RA.rotOverloads


----------------------------------------------------------------------------------------------------
--  ROTATION OPENERS                                                                              --
----------------------------------------------------------------------------------------------------
local openerNoCDs = {
	{ petAttack, function() return RA.Fetch("petmanagement", true) == true end, },
	{ {
		{ "Chimaera Shot", },
		{ "Barrage", {
			function() return RA.Fetch("nobarrage", false) == false end,
			function() return RA.Fetch("nocleave", false) == false end,
			"!target.ccinarea(20)",
			"target.raarea(10).enemies > 2",
		}, },
		{ "Aimed Shot", },
	}, "!player.channeling", },
}


local openerCDs = {
	{ petAttack, function() return RA.Fetch("petmanagement", true) == true end, },
	{ {
		{ "Stampede", },
		{ {
			{ "#trinket1", },
			{ "#trinket2", },
			{ "Blood Fury", },
			{ "Chimaera Shot", },
			{ "Berserking", },
			{ "Arcane Torrent", },
			{ "Aimed Shot", },
		}, "player.spell(Stampede).cooldown >= 260", },
	}, { "!player.channeling", "talent(5,3)", }, },
	{ {
		{ "A Murder of Crows", },
		{ {
			{ "#trinket1", },
			{ "#trinket2", },
			{ "Blood Fury", },
			{ "Chimaera Shot", },
			{ "Berserking", },
			{ "Arcane Torrent", },
			{ "Aimed Shot", },
		}, "target.debuff(A Murder of Crows)", },
	}, { "!player.channeling", "talent(5,1)", }, },
}










----------------------------------------------------------------------------------------------------
-- Rotations
----------------------------------------------------------------------------------------------------
local carefulaimsimc = {
	--actions.careful_aim=glaive_toss,if=active_enemies>2
	{ "Glaive Toss", { "target.raarea(10).enemies > 2", }, },
	--actions.careful_aim+=/powershot,if=active_enemies>1&cast_regen<focus.deficit
	{ "Powershot", {
		"target.raarea(10).enemies > 1",
		function() return
			RA.Eval("player.spell(Powershot).regen") < RA.Eval("player.power.deficit")
		end,
	}, },
	--actions.careful_aim+=/barrage,if=active_enemies>1
	{ "Barrage", { "target.raarea(20).enemies > 1", }, },
	--actions.careful_aim+=/aimed_shot
	{ "Aimed Shot", { "prioritize(Aimed Shot, 80)", }, },
	--actions.careful_aim+=/focusing_shot,if=50+cast_regen<focus.deficit
	{ "Focusing Shot", {
		function() return
			(50 + RA.Eval("player.spell(Focusing Shot).regen")) < RA.Eval("player.power.deficit")
		end,
	}, },
	--actions.careful_aim+=/steady_shot
	{ "Steady Shot", },
}

local carefulaimsmartbarrage = {
	--actions.careful_aim=glaive_toss,if=active_enemies>2
	{ "Glaive Toss", { "target.raarea(10).enemies > 2", }, },
	--actions.careful_aim+=/powershot,if=active_enemies>1&cast_regen<focus.deficit
	{ "Powershot", {
		"target.raarea(10).enemies > 1",
		function() return
			RA.Eval("player.spell(Powershot).regen") < RA.Eval("player.power.deficit")
		end,
	}, },
	--actions.careful_aim+=/barrage,if=active_enemies>1
	{ "Barrage", { "target.raarea(20).enemies > 1", "target.deathin >= 15", }, },
	--actions.careful_aim+=/aimed_shot
	{ "Aimed Shot", },
	--actions.careful_aim+=/focusing_shot,if=50+cast_regen<focus.deficit
	{ "Focusing Shot", {
		function() return
			(50 + RA.Eval("player.spell(Focusing Shot).regen")) < RA.Eval("player.power.deficit")
		end,
	}, },
	--actions.careful_aim+=/steady_shot
	{ "Steady Shot", },
}

local simc = {
	--{ "", { function() RA.Debug(0, "simc", "Rotation Entry Point") end, }, },
	--Focus pool if Rapid Fire almost off of cooldown

	--actions=auto_shot

	{ {
		--actions+=/use_item,name=[trinket]
		{ "#trinket1", { "player.buff(Berserking)", }, },
		{ "#trinket1", { "player.buff(Blood Fury)", }, },
		{ "#trinket1", { "player.buff(Rapid Fire)", }, },
		{ "#trinket2", { "player.buff(Berserking)", }, },
		{ "#trinket2", { "player.buff(Blood Fury)", }, },
		{ "#trinket2", { "player.buff(Rapid Fire)", }, },

		--actions+=/arcane_torrent,if=focus.deficit>=30
		{ "Arcane Torrent", { "player.focus.deficit >= 30", }, },

		--actions+=/blood_fury
		{ "Blood Fury", { "player.spell(Stampede).cooldown = 0", }, },
		{ "Blood Fury", { "player.spell(Stampede).cooldown > 120", }, },

		--actions+=/berserking
		{ "Berserking", { "player.spell(Stampede).cooldown = 0", }, },
		{ "Berserking", { "player.spell(Stampede).cooldown > 120", }, },

		--actions+=/potion,name=draenic_agility,if=((buff.rapid_fire.up|buff.bloodlust.up)&(cooldown.stampede.remains<1))|target.time_to_die<=25
		{ {
			{ "#109217", { "player.buff(Rapid Fire)", "player.spell(Stampede).cooldown = 0", }, },
			{ "#109217", { "player.bursthaste", "player.spell(Stampede).cooldown = 0", }, },
			{ "#109217", { "target.deathin <= 25", }, },
		}, { function() return RA.Fetch("agipotion", false) == true end, }, },

	}, { "!player.channeling", "modifier.cooldowns", }, },

	--actions+=/chimaera_shot
	{ "Chimaera Shot", {
		"!player.channeling",
		function() return RA.Fetch("nocleave", false) == false end,
	}, },

	--actions+=/kill_shot
	{ "Kill Shot", {
		"!player.channeling",
		"execute(Kill Shot)",
	}, },

	--actions+=/rapid_fire
	{ "Rapid Fire", { "modifier.cooldowns", }, },

	--actions+=/stampede,if=buff.rapid_fire.up|buff.bloodlust.up|target.time_to_die<=25
	{ {
		{ "Stampede", { "player.buff(Rapid Fire)", }, },
		{ "Stampede", { "player.bursthaste", }, },
		{ "Stampede", { "target.deathin <= 25", }, },
	}, { "!player.channeling", "modifier.cooldowns", }, },

	--actions+=/call_action_list,name=careful_aim,if=buff.careful_aim.up
	{ carefulaimsimc, { "@RA.CarefulAim()", }, },

	--actions+=/explosive_trap,if=active_enemies>1
	{ "Explosive Trap", {
		function() return RA.Fetch("nocleave", false) == false end,
		"target.raarea(10).enemies > 1",
		"!target.ccinarea(10)",
		"!player.channeling",
		"player.buff(Trap Launcher)",
	}, "target.ground", },

	--actions+=/a_murder_of_crows
	{ "A Murder of Crows", {
		"!player.channeling",
		"target.deathin > 60",
		"modifier.cooldowns",
	}, },
	{ "A Murder of Crows", {
		"!player.channeling",
		"target.deathin < 12",
	}, },

	--actions+=/dire_beast,if=cast_regen+action.aimed_shot.cast_regen<focus.deficit
	{ "Dire Beast", {
		"!player.channeling",
		function() return
			(RA.Eval("player.spell(Dire Beast).regen") + RA.Eval("player.spell(Aimed Shot).regen")) < RA.Eval("player.power.deficit")
		end,
	}, },

	--actions+=/glaive_toss
	{ "Glaive Toss", {
		"!player.channeling",
		function() return RA.Fetch("nocleave", false) == false end,
		"!target.ccinarea(10)",
	}, },

	--actions+=/powershot,if=cast_regen<focus.deficit
	{ "Powershot", {
		"!player.channeling",
		function() return RA.Fetch("nocleave", false) == false end,
		function() return RA.Eval("player.spell(Powershot).regen") < RA.Eval("player.power.deficit") end,
		"!target.ccinarea(10)",
	}, },

	--actions+=/barrage
	{ "Barrage", {
		function() return RA.Fetch("nobarrage", false) == false end,
		function() return RA.Fetch("nocleave", false) == false end,
		"!target.ccinarea(20)",
	}, },

	--actions+=/steady_shot,if=focus.deficit*cast_time%(14+cast_regen)>cooldown.rapid_fire.remains
	{ "Steady Shot", {
		"!player.channeling",
		function() return ((RA.Eval("player.power.deficit") * RA.Eval("player.spell(Steady Shot).casttime")) / (14 + RA.Eval("player.spell(Steady Shot).regen"))) > RA.Eval("player.spell(Rapid Fire).cooldownremains") end,
	}, },

	--actions+=/focusing_shot,if=focus.deficit*cast_time%(50+cast_regen)>cooldown.rapid_fire.remains&focus<100
	{ "Focusing Shot", {
		"!player.channeling",
		function() return ((RA.Eval("player.power.deficit") * RA.Eval("player.spell(Focusing Shot).casttime")) / (50 + RA.Eval("player.spell(Focusing Shot).regen"))) > RA.Eval("player.spell(Rapid Fire).cooldownremains") and RA.Eval("player.focus > 100") end,
	}, },

	--actions+=/steady_shot,if=buff.pre_steady_focus.up&(14+cast_regen+action.aimed_shot.cast_regen)<=focus.deficit
	{ "Steady Shot", {
		"!player.channeling",
		"lastcast(Steady Shot)",
		function() return (14 + RA.Eval("player.spell(Steady Shot).regen") + RA.Eval("player.spell(Aimed Shot).regen")) <= RA.Eval("player.power.deficit") end,
	}, },

	--actions+=/multishot,if=active_enemies>6
	{ "Multi-Shot", {
		"!player.channeling",
		function() return RA.Fetch("nocleave", false) == false end,
		"!target.ccinarea(8)",
		"target.raarea(10).enemies > 6",
	}, },

	--actions+=/aimed_shot,if=talent.focusing_shot.enabled
	{ "Aimed Shot", {
		"!player.channeling",
		"talent(7,2)",
	}, },

	--actions+=/aimed_shot,if=focus+cast_regen>=85
	{ "Aimed Shot", {
		"!player.channeling",
		function() return (RA.Eval("player.focus") + RA.Eval("player.spell(Aimed Shot).regen")) >= 85 end,
	}, },

	--actions+=/aimed_shot,if=buff.thrill_of_the_hunt.react&focus+cast_regen>=65
	{ "Aimed Shot", {
		"!player.channeling",
		"player.buff(Thrill of the Hunt)",
		function() return (RA.Eval("player.focus") + RA.Eval("player.spell(Aimed Shot).regen")) >= 65 end,
	}, },

	--actions+=/focusing_shot,if=50+cast_regen-10<focus.deficit
	{ "Focusing Shot", {
		"!player.channeling",
		function() return (50 + RA.Eval("player.spell(Focusing Shot).regen") - 10) < RA.Eval("player.power.deficit") end,
	}, },

	--actions+=/steady_shot
	{ "Steady Shot", { "!player.channeling", }, },
}


local smartbarrage = {
	--Focus pool if Rapid Fire almost off of cooldown

	--actions=auto_shot

	{ {
		--actions+=/use_item,name=[trinket]
		{ "#trinket1", { "player.buff(Berserking)", }, },
		{ "#trinket1", { "player.buff(Blood Fury)", }, },
		{ "#trinket1", { "player.buff(Rapid Fire)", }, },
		{ "#trinket2", { "player.buff(Berserking)", }, },
		{ "#trinket2", { "player.buff(Blood Fury)", }, },
		{ "#trinket2", { "player.buff(Rapid Fire)", }, },

		--actions+=/arcane_torrent,if=focus.deficit>=30
		{ "Arcane Torrent", { "player.focus.deficit >= 30", }, },

		--actions+=/blood_fury
		{ "Blood Fury", { "player.spell(Stampede).cooldown = 0", }, },
		{ "Blood Fury", { "player.spell(Stampede).cooldown > 120", }, },

		--actions+=/berserking
		{ "Berserking", { "player.spell(Stampede).cooldown = 0", }, },
		{ "Berserking", { "player.spell(Stampede).cooldown > 120", }, },

		--actions+=/potion,name=draenic_agility,if=((buff.rapid_fire.up|buff.bloodlust.up)&(cooldown.stampede.remains<1))|target.time_to_die<=25
		{ {
			{ "#109217", { "player.buff(Rapid Fire)", "player.spell(Stampede).cooldown = 0", }, },
			{ "#109217", { "player.bursthaste", "player.spell(Stampede).cooldown = 0", }, },
			{ "#109217", { "target.deathin <= 25", }, },
		}, { function() return RA.Fetch("agipotion", false) == true end, }, },

	}, { "!player.channeling", "modifier.cooldowns", }, },

	--actions+=/chimaera_shot
	{ "Chimaera Shot", {
		"!player.channeling",
		function() return RA.Fetch("nocleave", false) == false end,
	}, },

	--actions+=/kill_shot
	{ "Kill Shot", { "!player.channeling", "execute(Kill Shot)", }, },

	--actions+=/rapid_fire
	{ "Rapid Fire", { "modifier.cooldowns", }, },

	--actions+=/stampede,if=buff.rapid_fire.up|buff.bloodlust.up|target.time_to_die<=25
	{ {
		{ "Stampede", { "player.buff(Rapid Fire)", }, },
		{ "Stampede", { "player.bursthaste", }, },
		{ "Stampede", { "target.deathin <= 25", }, },
	}, { "!player.channeling", "modifier.cooldowns", }, },

	--actions+=/call_action_list,name=careful_aim,if=buff.careful_aim.up
	{ carefulaimsmartbarrage, { "target.carefulaim.up", }, },

	--actions+=/explosive_trap,if=active_enemies>1
	{ "Explosive Trap", {
		function() return RA.Fetch("nocleave", false) == false end,
		"target.raarea(10).enemies > 1",
		"!target.ccinarea(10)",
		"!player.channeling",
		"player.buff(Trap Launcher)",
	}, "target.ground", },

	--actions+=/a_murder_of_crows
	{ "A Murder of Crows", {
		"!player.channeling",
		"target.deathin > 60",
		"modifier.cooldowns",
	}, },
	{ "A Murder of Crows", {
		"!player.channeling",
		"target.deathin < 12",
	}, },

	--actions+=/dire_beast,if=cast_regen+action.aimed_shot.cast_regen<focus.deficit
	{ "Dire Beast", {
		"!player.channeling",
		function() return
			(RA.Eval("player.spell(Dire Beast).regen") + RA.Eval("player.spell(Aimed Shot).regen")) < RA.Eval("player.power.deficit")
		end,
	}, },

	--actions+=/glaive_toss
	{ "Glaive Toss", {
		"!player.channeling",
		function() return RA.Fetch("nocleave", false) == false end,
		"!target.ccinarea(10)",
	}, },

	--actions+=/powershot,if=cast_regen<focus.deficit
	{ "Powershot", {
		"!player.channeling",
		function() return RA.Fetch("nocleave", false) == false end,
		function() return RA.Eval("player.spell(Powershot).regen") < RA.Eval("player.power.deficit") end,
		"!target.ccinarea(10)",
	}, },

	--actions+=/barrage
	{ "Barrage", {
		function() return RA.Fetch("nobarrage", false) == false end,
		function() return RA.Fetch("nocleave", false) == false end,
		"!target.ccinarea(20)",
		"target.deathin >= 15",
	}, },

	--actions+=/steady_shot,if=focus.deficit*cast_time%(14+cast_regen)>cooldown.rapid_fire.remains
	{ "Steady Shot", {
		"!player.channeling",
		function() return ((RA.Eval("player.power.deficit") * RA.Eval("player.spell(Steady Shot).casttime")) / (14 + RA.Eval("player.spell(Steady Shot).regen"))) > RA.Eval("player.spell(Rapid Fire).cooldownremains") end,
	}, },

	--actions+=/focusing_shot,if=focus.deficit*cast_time%(50+cast_regen)>cooldown.rapid_fire.remains&focus<100
	{ "Focusing Shot", {
		"!player.channeling",
		function() return ((RA.Eval("player.power.deficit") * RA.Eval("player.spell(Focusing Shot).casttime")) / (50 + RA.Eval("player.spell(Focusing Shot).regen"))) > RA.Eval("player.spell(Rapid Fire).cooldownremains") and RA.Eval("player.focus > 100") end,
	}, },

	--actions+=/steady_shot,if=buff.pre_steady_focus.up&(14+cast_regen+action.aimed_shot.cast_regen)<=focus.deficit
	{ "Steady Shot", {
		"!player.channeling",
		"lastcast(Steady Shot)",
		function() return (14 + RA.Eval("player.spell(Steady Shot).regen") + RA.Eval("player.spell(Aimed Shot).regen")) <= RA.Eval("player.power.deficit") end,
	}, },

	--actions+=/multishot,if=active_enemies>6
	{ "Multi-Shot", {
		"!player.channeling",
		function() return RA.Fetch("nocleave", false) == false end,
		"!target.ccinarea(8)",
		"target.raarea(10).enemies > 6",
	}, },

	--actions+=/aimed_shot,if=talent.focusing_shot.enabled
	{ "Aimed Shot", {
		"!player.channeling",
		"talent(7,2)",
	}, },

	--actions+=/aimed_shot,if=focus+cast_regen>=85
	{ "Aimed Shot", {
		"!player.channeling",
		function() return (RA.Eval("player.focus") + RA.Eval("player.spell(Aimed Shot).regen")) >= 85 end,
	}, },

	--actions+=/aimed_shot,if=buff.thrill_of_the_hunt.react&focus+cast_regen>=65
	{ "Aimed Shot", {
		"!player.channeling",
		"player.buff(Thrill of the Hunt)",
		function() return (RA.Eval("player.focus") + RA.Eval("player.spell(Aimed Shot).regen")) >= 65 end,
	}, },

	--actions+=/focusing_shot,if=50+cast_regen-10<focus.deficit
	{ "Focusing Shot", {
		"!player.channeling",
		function() return (50 + RA.Eval("player.spell(Focusing Shot).regen") - 10) < RA.Eval("player.power.deficit") end,
	}, },

	--actions+=/steady_shot
	{ "Steady Shot", { "!player.channeling", }, },
}


local azortharion = {
	-- Rotation --------------------------------------------------------------------------------
	{ "Rapid Fire", { "modifier.cooldowns", }, },

	{ "Chimaera Shot", { "!player.channeling", }, },
	{ "Kill Shot", { "!player.channeling", "execute(Kill Shot)", }, },

	{ {
		{ "Glaive Toss", {
			function() return RA.Fetch("nocleave", false) == false end,
			"!target.ccinarea(10)",
		}, },
		{ "Barrage", {
			function() return RA.Fetch("nobarrage", false) == false end,
			function() return RA.Fetch("nocleave", false) == false end,
			"!target.ccinarea(20)",
		}, },
	}, { "target.raarea(20).enemies > 1", }, },

	-- Careful Aim -------------------------------------------------------------------------
	{ {
		{ "Aimed Shot", { "target.health > 80", }, },
		{ "Aimed Shot", { "player.buff(Rapid Fire)", }, },
		{ "Aimed Shot", { "player.buff(Thrill of the Hunt)", }, },
		{ "Aimed Shot", { "player.proc.crit", }, },
		{ "Steady Shot", { "target.health > 80", }, },
		{ "Steady Shot", { "player.buff(Rapid Fire)", }, },
		{ "Focusing Shot", { "target.health > 80", }, },
		{ "Focusing Shot", { "player.buff(Rapid Fire)", }, },
	}, { "!player.channeling", }, },
	----------------------------------------------------------------------------------------

	{ "Glaive Toss", {
		function() return RA.Fetch("nocleave", false) == false end,
		"!target.ccinarea(10)",
	}, },
	{ "Barrage", {
		function() return RA.Fetch("nobarrage", false) == false end,
		function() return RA.Fetch("nocleave", false) == false end,
		"!target.ccinarea(20)",
	}, },

	-- Less than 7 Targets -----------------------------------------------------------------
	{ "Aimed Shot", {
		"!player.channeling",
		"player.focus > 70",
		"target.raarea(10).enemies < 7",
	}, },
	-- More than 7 Targets -----------------------------------------------------------------
	{ "Multi-Shot", {
		"!player.channeling",
		"target.raarea(10).enemies >= 7",
	}, },
	----------------------------------------------------------------------------------------

	{ "Steady Shot", { "!player.channeling", }, },
	{ "Focusing Shot", { "!player.channeling", }, },

	-- Racials and Trinkets --------------------------------------------------------------------
	{ {
		{ "Arcane Torrent", { "player.focus.deficit >= 30", }, },

		{ "Blood Fury", { "player.spell(Stampede).cooldown = 0", }, },
		{ "Blood Fury", { "player.spell(Stampede).cooldown > 120", }, },
		{ "Blood Fury", { "player.buff(Rapid Fire)", }, },

		{ "Berserking", { "player.spell(Stampede).cooldown = 0", }, },
		{ "Berserking", { "player.spell(Stampede).cooldown > 120", }, },
		{ "Berserking", { "player.buff(Rapid Fire)", }, },

		{ "#trinket1", { "player.buff(Berserking)", }, },
		{ "#trinket1", { "player.buff(Blood Fury)", }, },
		{ "#trinket1", { "player.buff(Rapid Fire)", }, },

		{ "#trinket2", { "player.buff(Berserking)", }, },
		{ "#trinket2", { "player.buff(Blood Fury)", }, },
		{ "#trinket2", { "player.buff(Rapid Fire)", }, },
	}, {
		"!player.channeling",
		"modifier.cooldowns",
	}, },


	-- Agility Potion --------------------------------------------------------------------------
	{ {
		{ "#109217", {
			"!talent(5,3)",
			"player.buff(Rapid Fire)",
			"target.health <= 20",
		}, },
		{ "#109217", {
			"talent(5,3)",
			"player.spell(Stampede).cooldown >= 260",
			"player.hashero",
		}, },
		{ "#109217", {
			"talent(5,3)",
			"player.spell(Stampede).cooldown >= 260",
			"player.buff(Rapid Fire)",
		}, },
		{ "#109217", {
			"target.deathin <= 25",
		}, },
	}, {
		"!player.channeling",
		"modifier.cooldowns",
		function() return RA.Fetch("agipotion", false) == true end,
	}, },


	-- Stampede --------------------------------------------------------------------------------
	{ {
		{ "Stampede", { "player.hashero", }, },
		{ "Stampede", { "player.buff(Rapid Fire)", }, },
	}, { "!player.channeling", "modifier.cooldowns", "talent(5,3)", }, },
}
















----------------------------------------------------------------------------------------------------
--  ROTATION                                                                                      --
----------------------------------------------------------------------------------------------------
ProbablyEngine.rotation.register_custom(254, "RA - Marksmanship",
-- Combat
{
	--{ "", { function() print("Entry point into PE Rotation!") end, }, },
	--{ "", { function() RA.Debug(0, "Combat", "Entry point PE Rotation") end, }, },
	-- Pause ---------------------------------------------------------------------------------------
	{ "pause", { "@RA.Pause()", }, },
	{ "pause", { "player.casting", }, },
	{ "pause", { "player.channeling", }, },


	{ "/cancelaura " .. select(1, GetSpellInfo(5118)), {
		function() return RA.Fetch("aspectincombat", true) == false end,
		"player.buff(Aspect of the Cheetah)",
		"!player.glyph(Glyph of Aspect of the Cheetah)",
	}, },

	{ queueSpells, },


	-- Openers -------------------------------------------------------------------------------------
	{ {
		{ openerNoCDs, "!modifier.cooldowns", },
		{ openerCDs, "modifier.cooldowns", },
	}, { "player.time < 5", }, },


	-- Rotation ------------------------------------------------------------------------------------
	{ {
		{ defensives, function() return RA.Fetch("defensives", true) == true end, },
		{ miscellaneousCombat, },
		{ misdirection, function() return RA.Fetch("misdirects", true) == true end, },
		{ petAttack, function() return RA.Fetch("petmanagement", true) == true end, },
		{ petManagementCombat, function() return RA.Fetch("petmanagement", true) == true end, },
		{ poolFocusFocusingShot, },
		{ poolFocusSteadyShot, },

		{ azortharion, { function() return RA.Fetch('combatrotation', 'simc') == 'azortharion' end, }, },
		{ simc, { function() return RA.Fetch('combatrotation', 'simc') == 'simc' end, }, },
		{ smartbarrage, { function() return RA.Fetch('combatrotation', 'simc') == 'smartbarrage' end, }, },

	}, { "player.time > 5", }, },
},


-- Out of Combat
{
	{ queueSpells, },
	{ miscellaneousOOC, },
	{ petManagementOOC, function() return RA.Fetch("petmanagement", true) == true end, },
	{ petSummon, },
},


-- Callback
function()
	RA.BaseStatsInit()
	RA.HunterPetSlots()

	-- Splash Logo
	RA.SplashInit()

	function RA.OptionsWindowHMMBuild()
		RA.optionsWindowHMM = ProbablyEngine.interface.buildGUI({
			key = 'ra_config_h_mm',
			title = 'RotAgent',
			subtitle = 'Marksmanship',
			profiles = true,
			width = 275,
			height = 500,
			color = "4e7300",
			config = {
				{ type = 'rule', },
				{ type = 'header', text = 'Basics', },
				{ type = 'rule', },

				{ type = 'dropdown', key = 'combatrotation', text = 'Combat Rotation Logic', list = {
					{ key = 'azortharion', text = 'Azortharion Guide' },
					{ key = 'simc', text = 'SimC APL' },
					{ key = 'smartbarrage', text = 'Smart Barrage' },
				}, default = 'simc', },
				{ type = 'checkbox', key = 'autotarget', text = 'Auto Target Logic', default = true, },
				{ type = 'dropdown', key = 'autotargetalgorithm', text = 'Auto Target Algorithm', list = {
					{ key = 'highest', text = 'Highest HP' },
					{ key = 'lowest', text = 'Lowest HP' },
					{ key = 'nearest', text = 'Nearest' },
					{ key = 'cascade', text = 'Cascade' },
				}, default = 'highest', },
				{ type = 'checkbox', key = 'autocleartarget', text = 'Auto Clear Target if not Enemy Unit.', default = true, },
				{ type = 'checkbox', key = 'autotraplauncher', text = 'Auto Trap Launcher Logic', default = true, },
				{ type = 'checkbox', key = 'autoaspect', text = 'Auto Aspect of the Cheetah', default = false, },
				{ type = 'checkbox', key = 'bosslogic', text = 'Boss Logic', default = true, },
				{ type = 'checkbox', key = 'agipotion', text = 'Agility Potion Logic', default = false, },
				{ type = 'checkbox', key = 'defensives', text = 'Defensive Logic', default = true, },
				{ type = 'checkbox', key = 'misdirects', text = 'Misdirection Logic', default = true, },
				{ type = 'checkbox', key = 'pause', text = 'Pause on Keybind', default = true, },
				{ type = 'checkbox', key = 'petmanagement', text = 'Pet Management', default = true, },
				{ type = 'checkbox', key = 'prioritynuke', text = 'Priority Nuke target', default = false, },
				{ type = 'checkbox', key = 'nobarrage', text = 'No Barrage (prevent Barrage use)', default = false },
				{ type = 'checkbox', key = 'nocleave', text = 'No Cleave (prevent any AoE)', default = false },
				{ type = 'checkbox', key = 'summonpet', text = 'Summon Pet', default = true, },
				{ type = 'dropdown', key = 'summonslot', text = 'Which Pet to Summon', list = {
					{ key = 'slot1', text = tostring(RA.hunterPetSlot[1]) },
					{ key = 'slot2', text = tostring(RA.hunterPetSlot[2]) },
					{ key = 'slot3', text = tostring(RA.hunterPetSlot[3]) },
					{ key = 'slot4', text = tostring(RA.hunterPetSlot[4]) },
					{ key = 'slot5', text = tostring(RA.hunterPetSlot[5]) },
				}, default = 'slot1', },
				{ type = 'spinner', key = 'executevalue', text = 'Execute range percentage |cffaaaaaaHP < %|r', default = 35, },

				{ type = 'rule', },
				{ type = 'header', text = 'Advanced',},
				{ type = 'rule', },

				{ type = 'header', text = 'Unit Sniping',},
				{ type = 'checkbox', key = 'amocsnipe', text = 'Use AMoC on low health Units (to reset).', default = true },
				{ type = 'checkbox', key = 'killshotsnipe', text = 'Use Kill Shot on low health Units.', default = true },
				{ type = 'rule', },

				{ type = 'header', text = 'Aspect of the Cheetah',},
				{ type = 'checkbox', key = 'aspectincombat', text = 'Leave Aspect of the Cheetah on in combat?', default = true },
				{ type = 'spinner', key = 'aspectmovingfor', text = 'Aspect after Moving for |cffaaaaaaX seconds|r', min = 0, max = 10, step = 1, default = 2, desc = 'Aspect of the Cheetah will be cast if the player has been moving for X seconds. If X is 0 the cast will be instant. Aspect will cancel upon entering combat if Glyph of Aspect of the Cheetah is not active.' },
				{ type = 'rule', },

				{ type = 'header', text = 'Boss Logic' },
				{ type = 'checkbox', key = 'brackflamethrower', text = 'Turn off Flamethrower if activated', default = true },
				{ type = 'checkspin', key = 'fdinfestingsporestacks', text = 'Feign Death Infesting Spores |cffaaaaaaStacks >|r', default_check = true, default_spin = 6, },
				{ type = 'rule', },

				{ type = 'header', text = 'Camouflage' },
				{ type = 'checkbox', key = 'camouflage', text = 'Use Camouflage if Glyph of Camouflage enabled?', default = true },
				{ type = 'rule', },

				{ type = 'header', text = 'Defensive Logic' },
				{ type = 'spinner', key = 'exhilaration', text = 'Exhilaration at |cffaaaaaaHP < %|r', default = 60, },
				{ type = 'spinner', key = 'deterrence', text = 'Deterrence at |cffaaaaaaHP < %|r', default = 10 },
				{ type = 'spinner', key = 'healthpot', text = 'Health Potion/Stone at |cffaaaaaaHP < %|r', default = 40, },
				{ type = 'checkbox', key = 'masterscall', text = 'Master\'s Call slows?', default = false },
				{ type = 'rule' },

				{ type = 'header', text = 'Miscellaneous' },
				{ type = 'checkbox', key = 'concussiveshot', text = 'Concussive Shot moving targets', default = true, },
				{ type = 'checkbox', key = 'tranqshot', text = 'Tranquilize dispellable buffs', default = false, desc = 'Automatically attempt to remove dispellabe Magic or Enrage effects with Tranquilizing Shot.', },
				{ type = 'rule', },

				{ type = 'header', text = 'Misdirection' },
				{ type = 'spinner', key = 'mdfocusagro', text = 'Misdirect to Focus at |cffaaaaaaX% aggro|r', default = 50, },
				{ type = 'dropdown', key = 'mdkeybind', text = 'Misdirection Keybind', list = {
					{ key = 'lalt', text = 'lalt' },
					{ key = 'lcontrol', text = 'lcontrol' },
					{ key = 'lshift', text = 'lshift' },
					{ key = 'ralt', text = 'ralt' },
					{ key = 'rcontrol', text = 'rcontrol' },
					{ key = 'rshift', text = 'rshift' },
				}, default = 'lalt', desc = 'Misdirect to Focus Target, if no Focus Misdirect to Mouseover Target', },
				{ type = 'rule' },

				{ type = 'header', text = 'Pause',},
				{ type = 'dropdown', key = 'pausekeybind', text = 'Pause Keybind', list = {
					{ key = 'lalt', text = 'lalt' },
					{ key = 'lcontrol', text = 'lcontrol' },
					{ key = 'lshift', text = 'lshift' },
					{ key = 'ralt', text = 'ralt' },
					{ key = 'rcontrol', text = 'rcontrol' },
					{ key = 'rshift', text = 'rshift' },
				}, default = 'lshift' },
				{ type = 'rule', },

				{ type = 'header', text = 'Pet Management' },
				{ type = 'spinner', key = 'petmend', text = 'Mend Pet at |cffaaaaaaHP < %|r', default = 95, },
				{ type = 'spinner', key = 'petdash', text = 'Use Dash if |cffaaaaaaTarget > Distance|r', min = 1, max = 40, step = 1, default = 15, },
				{ type = 'rule' },

				{ type = 'header', text = 'Pool Focus',},
				{ type = 'dropdown', key = 'poolfocuskeybind', text = 'Focus pooling Keybind', list = {
					{ key = 'lalt', text = 'lalt' },
					{ key = 'lcontrol', text = 'lcontrol' },
					{ key = 'lshift', text = 'lshift' },
					{ key = 'ralt', text = 'ralt' },
					{ key = 'rcontrol', text = 'rcontrol' },
					{ key = 'rshift', text = 'rshift' },
				}, default = 'rcontrol' },
				{ type = 'rule' },

				{ type = 'header', text = 'Debug', },
				{ type = 'checkspin', key = 'debug', text = 'Debug output, set level to the right.', min = 0, max = 5, step = 1, default = 0, },
				{ type = 'rule' },

				{ type = 'header', text = 'Extra' },
				{ type = 'checkbox', key = 'autolfg', text = 'Auto LFG Accept', default = false, },
				{ type = "checkbox", key = 'splash', text = "Splash Image", default = true, },
			}
		})
		RA.optionsWindowHMM.parent:Hide()
	end
	RA.OptionsWindowHMMBuild()

	ProbablyEngine.buttons.create(
		'config', 'Interface\\ICONS\\Inv_misc_gear_01',
		function(self)
			if self.checked then
				ProbablyEngine.buttons.setInactive('config')
				RA.OptionsWindowShow()
			else
				ProbablyEngine.buttons.setActive('config')
				RA.OptionsWindowShow()
			end
		end,
		'Configure', 'Change how the rotation behaves.'
	)
	ProbablyEngine.buttons.setInactive('config')

	ProbablyEngine.buttons.create(
		'nobarrage', 'Interface\\ICONS\\ability_hunter_rapidregeneration',
		function(self)
			if self.checked then
				ProbablyEngine.buttons.setInactive('nobarrage')
				self.checked = false
				RA.Write('nobarrage', false)
				RA.optionsWindowHMM = nil
				RA.OptionsWindowHMMBuild()
				RA.warningFrameBarrage:message("Barrage On!")
			else
				ProbablyEngine.buttons.setActive('nobarrage')
				self.checked = true
				RA.Write('nobarrage', true)
				RA.optionsWindowHMM = nil
				RA.OptionsWindowHMMBuild()
				RA.warningFrameBarrage:message("Barrage Off!")
			end
		end,
		'No Barrage', 'Prevents the use Barrage.'
	)
	if RA.Fetch("nobarrage", false) == false then
		ProbablyEngine.buttons.setInactive("nobarrage")
	end

	ProbablyEngine.buttons.create(
		'nocleave', 'Interface\\ICONS\\Spell_shadow_rainoffire',
		function(self)
			if self.checked then
				ProbablyEngine.buttons.setInactive('nocleave')
				self.checked = false
				RA.Write('nocleave', false)
				RA.optionsWindowHMM = nil
				RA.OptionsWindowHMMBuild()
				RA.warningFrameCleave:message("Cleave On!")
			else
				ProbablyEngine.buttons.setActive('nocleave')
				self.checked = true
				RA.Write('nocleave', true)
				RA.optionsWindowHMM = nil
				RA.OptionsWindowHMMBuild()
				RA.warningFrameCleave:message("Cleave Off!")
			end
		end,
		'No Cleave', 'Prevents the use of all multi-target abilities.'
	)
	if RA.Fetch("nocleave", false) == false then
		ProbablyEngine.buttons.setInactive("nocleave")
	end
end)